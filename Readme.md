# Luxoft recruitment task

## Description:
Simple app which allows presenting current currency rates for USD and PLN.

App utilize libraries like:

- Dagger2
- RxJava2
- ViewModel
- Retrofit
- MockWebServer
- JUnit
- Mockito

## App assumptions:
1. Refresh rate is independent of user interaction.
1. Refresh rate should be easy to control and test.
1. App allows change screen orientation.
1. When there is no internet connection app should present error.
1. When internet connection is re-established app should automatically update UI.
