package com.cisek.luxofttask

import com.cisek.luxofttask.data.models.CurrencyRates
import com.cisek.luxofttask.data.models.Rates

object DataFactory {

    fun createCurrencyRates(usd: Double, pln: Double): CurrencyRates {
        return CurrencyRates("", "", Rates(USD = usd, PLN = pln))
    }
}