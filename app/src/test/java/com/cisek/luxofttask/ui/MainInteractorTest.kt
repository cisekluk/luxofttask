package com.cisek.luxofttask.ui

import com.cisek.luxofttask.DataFactory.createCurrencyRates
import com.cisek.luxofttask.data.ApiService
import com.cisek.luxofttask.data.connectivity.NoNetworkException
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

class MainInteractorTest {
    private val apiService: ApiService = mock()

    private val interactor = MainInteractor(apiService, 1, TimeUnit.MILLISECONDS)

    private val schedulers = TestScheduler()

    @Before
    fun setUp() {
        RxJavaPlugins.setComputationSchedulerHandler { schedulers }
    }

    @After
    fun tearDown() {
        RxJavaPlugins.reset()
    }

    @Test
    fun `when new currency rates was emitted then map currency rates to Data state `() {
        val firstItem = createCurrencyRates(0.1, 5.5)
        val secondItem = createCurrencyRates(2.1, 5.2)
        val thirdItem = createCurrencyRates(2.8, 0.0)

        whenever(apiService.latestCurrencyConversion())
            .thenReturn(Single.just(firstItem))
            .thenReturn(Single.just(secondItem))
            .thenReturn(Single.just(thirdItem))

        val testObserver = interactor.observe().test()

        schedulers.advanceTimeTo(10, TimeUnit.SECONDS)

        testObserver.assertValueAt(0, MainPartialState.Data(firstItem))
        testObserver.assertValueAt(1, MainPartialState.Data(secondItem))
        testObserver.assertValueAt(2, MainPartialState.Data(thirdItem))
        testObserver.assertNotComplete()
    }

    @Test
    fun `when NoNetworkException is emitted then map exception to NoInternet state`() {
        whenever(apiService.latestCurrencyConversion())
            .thenReturn(Single.error(NoNetworkException()))

        interactor.observe().test()
            .assertValue { it is MainPartialState.NoInternet }
            .assertNotComplete()
    }

    @Test
    fun `when emitted exception is different than NoNetworkException then emit DefaultError state`() {
        whenever(apiService.latestCurrencyConversion())
            .thenReturn(Single.error(Exception()))

        interactor.observe().test()
            .assertValue { it is MainPartialState.DefaultError }
            .assertNotComplete()
    }

}