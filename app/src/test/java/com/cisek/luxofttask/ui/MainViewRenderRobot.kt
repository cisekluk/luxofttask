package com.cisek.luxofttask.ui

import com.cisek.luxofttask.base.ViewRender
import org.junit.Assert

class MainViewRenderRobot(viewModel: MainViewModel) {

    private val renderedStates = arrayListOf<MainPartialState>()

    private val viewRender = object : ViewRender<MainPartialState> {
        override fun renderState(partialState: MainPartialState) {
            renderedStates.add(partialState)
        }
    }

    init {
        viewModel.attachView(viewRender)
    }

    fun assertViewStatesRendered(vararg expectedStates: MainPartialState) {
        Assert.assertEquals(expectedStates.toCollection(arrayListOf()), renderedStates)
    }
}