package com.cisek.luxofttask.ui

import com.cisek.luxofttask.DataFactory.createCurrencyRates
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.junit.Before
import org.junit.Test

class MainViewModelTest {

    private val interactor: MainInteractor = mock()
    private val partialStates = PublishSubject.create<MainPartialState>()

    private val viewModel = MainViewModel(interactor)
    private val viewRobot = MainViewRenderRobot(viewModel)

    @Before
    fun setUp() {
        whenever(interactor.observe()).thenReturn(partialStates)

        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun `render no internet and data state on the view when occurred`() {
        viewModel.bind()
        val data = createCurrencyRates(1.1, 0.5)

        partialStates.onNext(MainPartialState.NoInternet)
        partialStates.onNext(MainPartialState.Data(data))

        viewRobot.assertViewStatesRendered(
            MainPartialState.NoInternet,
            MainPartialState.Data(data)
        )
    }

    @Test
    fun `when exception occured render default error`() {
        viewModel.bind()

        partialStates.onError(Exception())

        viewRobot.assertViewStatesRendered(
            MainPartialState.DefaultError
        )
    }
}