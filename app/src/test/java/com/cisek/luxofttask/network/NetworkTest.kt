package com.cisek.luxofttask.network

import com.cisek.luxofttask.data.ApiService
import com.cisek.luxofttask.data.connectivity.ConnectivityMonitor
import com.cisek.luxofttask.data.connectivity.NoNetworkException
import com.cisek.luxofttask.di.modules.NetworkModule
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Test
import java.net.HttpURLConnection

class NetworkTest {
    private val connectivityMonitor: ConnectivityMonitor = mock {
        whenever(it.isConnected()).thenReturn(true)
    }

    private val url = "/"
    private val mockWebServer = MockWebServer()
    private val networkModule = NetworkModule()

    private val gson = networkModule.providesGson()
    private val loggingInterceptor = networkModule.providesHttpLoggingInterceptor()
    private val noInternetInterceptor =
        networkModule.providesNetworkConnectionInterceptor(connectivityMonitor)

    private var apiService: ApiService = networkModule.providesRetrofit(
        networkModule.providesGsonConverterFactory(gson),
        networkModule.providesOkHttpClient(loggingInterceptor, noInternetInterceptor),
        networkModule.providesRxJavaCallAdapterFactory(),
        mockWebServer.url(url).toString()
    ).create(ApiService::class.java)

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun `verify when server return response data are converted`() {
        val json = Helper.getJsonFromPath("json/currency_conversion.json")
        val response = MockResponse().setResponseCode(HttpURLConnection.HTTP_OK).setBody(json)
        mockWebServer.enqueue(response)

        val apiResponse = apiService.latestCurrencyConversion().test()

        apiResponse.assertValue { it.rates.PLN == 4.3191 }
        apiResponse.assertNoErrors()
    }

    @Test
    fun `verify when no internet exception is throw`() {
        whenever(connectivityMonitor.isConnected()).thenReturn(false)
        val response = MockResponse().setResponseCode(HttpURLConnection.HTTP_OK)
        mockWebServer.enqueue(response)

        val apiResponse = apiService.latestCurrencyConversion().test()

        apiResponse.assertError { it is NoNetworkException }
    }
}