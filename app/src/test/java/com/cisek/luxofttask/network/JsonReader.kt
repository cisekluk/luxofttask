package com.cisek.luxofttask.network

import java.io.File

object Helper {

    fun getJsonFromPath(path: String): String {
        val uri = this.javaClass.classLoader.getResource(path)
        val file = File(uri.path)
        return String(file.readBytes())
    }
}