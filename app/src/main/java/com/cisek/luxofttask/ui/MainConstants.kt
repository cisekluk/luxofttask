package com.cisek.luxofttask.ui

import java.util.concurrent.TimeUnit

val REFRESH_PERIOD = 1L
val REFRESH_TIME_UNIT = TimeUnit.MINUTES