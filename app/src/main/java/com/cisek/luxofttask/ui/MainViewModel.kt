package com.cisek.luxofttask.ui

import com.cisek.luxofttask.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(private val interactor: MainInteractor) :
    BaseViewModel<MainPartialState>() {

    override fun bind() {
        compositeDisposable.add(interactor.observe()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .distinctUntilChanged()
            .subscribe(
                {
                    viewRender.renderState(it)
                }, {
                    viewRender.renderState(MainPartialState.DefaultError)
                }
            )
        )
    }
}
