package com.cisek.luxofttask.ui

import com.cisek.luxofttask.data.models.CurrencyRates

sealed class MainPartialState {

    data class Data(val data: CurrencyRates) : MainPartialState()

    object NoInternet : MainPartialState()

    object DefaultError : MainPartialState()
}