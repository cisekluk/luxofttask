package com.cisek.luxofttask.ui

import com.cisek.luxofttask.data.ApiService
import com.cisek.luxofttask.data.connectivity.NoNetworkException
import com.cisek.luxofttask.di.modules.MainModule
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named

class MainInteractor @Inject constructor(
    private val apiService: ApiService,
    @Named(MainModule.PERIOD) private val period: Long,
    @Named(MainModule.TIME_UNIT) private val timeUnit: TimeUnit
) {

    fun observe(): Observable<MainPartialState> {
        return Observable.interval(period, timeUnit)
            .flatMapSingle { api() }
            .startWith(api().toObservable())
    }

    private fun api(): Single<MainPartialState> {
        return apiService.latestCurrencyConversion()
            .map<MainPartialState> { MainPartialState.Data(it) }
            .onErrorReturn { mapExceptionToPartialState(it) }
    }

    private fun mapExceptionToPartialState(throwable: Throwable): MainPartialState {
        return when (throwable) {
            is NoNetworkException -> MainPartialState.NoInternet
            else -> MainPartialState.DefaultError
        }
    }
}