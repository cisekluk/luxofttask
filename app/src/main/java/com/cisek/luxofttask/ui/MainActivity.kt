package com.cisek.luxofttask.ui

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.cisek.luxofttask.R
import com.cisek.luxofttask.base.BaseActivity
import com.cisek.luxofttask.base.ViewModelFactory
import com.cisek.luxofttask.base.ViewRender
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.pln
import kotlinx.android.synthetic.main.activity_main.root
import kotlinx.android.synthetic.main.activity_main.usd
import kotlinx.android.synthetic.main.item.view.title
import kotlinx.android.synthetic.main.item.view.value
import javax.inject.Inject

class MainActivity : BaseActivity<MainPartialState, MainViewModel>(), ViewRender<MainPartialState> {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val noInternerError by lazy {
        Snackbar.make(
            root,
            getString(R.string.error_no_internet),
            Snackbar.LENGTH_INDEFINITE
        )
    }
    private val defaultError by lazy {
        Snackbar.make(
            root,
            getString(R.string.error_default),
            Snackbar.LENGTH_INDEFINITE
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        usd.title.text = getString(R.string.usd_title)
        pln.title.text = getString(R.string.pln_title)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        viewModel.attachView(this)
    }

    override fun renderState(partialState: MainPartialState) {
        when (partialState) {
            is MainPartialState.Data -> {
                hideError()
                usd.value.text = partialState.data.rates.USD.toString()
                pln.value.text = partialState.data.rates.PLN.toString()
            }
            is MainPartialState.NoInternet -> {
                hideError()
                noInternerError.show()
            }
            is MainPartialState.DefaultError -> {
                hideError()
                defaultError.show()
            }
        }
    }

    private fun hideError() {
        if (noInternerError.isShown) noInternerError.dismiss()
        if (defaultError.isShown) defaultError.dismiss()
    }
}