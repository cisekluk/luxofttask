package com.cisek.luxofttask.data

import com.cisek.luxofttask.data.models.CurrencyRates
import io.reactivex.Single
import retrofit2.http.GET

interface ApiService {

    @GET("latest")
    fun latestCurrencyConversion(): Single<CurrencyRates>
}