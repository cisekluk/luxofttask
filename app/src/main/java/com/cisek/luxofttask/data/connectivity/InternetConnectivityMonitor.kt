package com.cisek.luxofttask.data.connectivity

import android.content.Context
import android.net.ConnectivityManager
import javax.inject.Inject

class InternetConnectivityMonitor @Inject constructor(private val context: Context) :
    ConnectivityMonitor {

    override fun isConnected(): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }
}