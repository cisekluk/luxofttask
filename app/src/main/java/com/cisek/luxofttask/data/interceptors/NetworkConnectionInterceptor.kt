package com.cisek.luxofttask.data.interceptors

import com.cisek.luxofttask.data.connectivity.ConnectivityMonitor
import com.cisek.luxofttask.data.connectivity.NoNetworkException
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class NetworkConnectionInterceptor @Inject constructor(private val networkMonitor: ConnectivityMonitor) :
    Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return if (networkMonitor.isConnected()) {
            chain.proceed(chain.request())
        } else {
            throw NoNetworkException()
        }
    }
}