package com.cisek.luxofttask.data.connectivity

import java.lang.Exception

class NoNetworkException : Exception("No internet connection")