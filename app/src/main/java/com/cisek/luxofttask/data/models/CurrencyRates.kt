package com.cisek.luxofttask.data.models

data class CurrencyRates(
    val base: String,
    val date: String,
    val rates: Rates
)