package com.cisek.luxofttask.data.connectivity

interface ConnectivityMonitor {
    fun isConnected(): Boolean
}