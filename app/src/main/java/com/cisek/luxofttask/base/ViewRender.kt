package com.cisek.luxofttask.base

interface ViewRender<T> {

    fun renderState(partialState: T)
}