package com.cisek.luxofttask.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cisek.luxofttask.ui.MainInteractor
import com.cisek.luxofttask.ui.MainViewModel
import javax.inject.Inject

class ViewModelFactory @Inject constructor(private val interactor: MainInteractor) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {
            MainViewModel::class.java -> MainViewModel(interactor) as T
            else -> throw UnsupportedOperationException("Unknown view model type!")
        }
    }
}