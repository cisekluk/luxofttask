package com.cisek.luxofttask.base

import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity<E, T: BaseViewModel<E>> : DaggerAppCompatActivity() {

    lateinit var viewModel: T

    override fun onStart() {
        super.onStart()
        viewModel.bind()
    }

    override fun onStop() {
        super.onStop()
        viewModel.unbind()
    }
}