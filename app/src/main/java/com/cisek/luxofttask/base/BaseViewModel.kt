package com.cisek.luxofttask.base

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel<T> : ViewModel() {

    protected val compositeDisposable = CompositeDisposable()
    protected lateinit var viewRender: ViewRender<T>

    abstract fun bind()

    fun attachView(view: ViewRender<T>){
        this.viewRender = view
    }

    fun unbind() {
        compositeDisposable.clear()
    }
}