package com.cisek.luxofttask

import com.cisek.luxofttask.di.components.DaggerLuxoftTaskComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class LuxoftTaskApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerLuxoftTaskComponent.factory().create(this)
    }
}