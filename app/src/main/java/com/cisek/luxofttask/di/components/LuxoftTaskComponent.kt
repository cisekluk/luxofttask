package com.cisek.luxofttask.di.components

import com.cisek.luxofttask.LuxoftTaskApplication
import com.cisek.luxofttask.di.modules.InjectorModule
import com.cisek.luxofttask.di.modules.LuxoftTaskModule
import com.cisek.luxofttask.di.modules.MainModule
import com.cisek.luxofttask.di.modules.NetworkModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        InjectorModule::class,
        LuxoftTaskModule::class,
        NetworkModule::class,
        MainModule::class]
)
interface LuxoftTaskComponent : AndroidInjector<LuxoftTaskApplication> {

    @Component.Factory
    abstract class Builder : AndroidInjector.Factory<LuxoftTaskApplication>
}