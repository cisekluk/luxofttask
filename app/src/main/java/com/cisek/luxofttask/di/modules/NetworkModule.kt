package com.cisek.luxofttask.di.modules

import android.content.Context
import com.cisek.luxofttask.BuildConfig
import com.cisek.luxofttask.data.ApiService
import com.cisek.luxofttask.data.connectivity.ConnectivityMonitor
import com.cisek.luxofttask.data.connectivity.InternetConnectivityMonitor
import com.cisek.luxofttask.data.interceptors.NetworkConnectionInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Provides
    @Singleton
    fun providesGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun providesGsonConverterFactory(gson: Gson) = GsonConverterFactory.create(gson)

    @Provides
    @Singleton
    fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor()
        .apply { level = loggingLevel() }

    private fun loggingLevel() = if (BuildConfig.DEBUG) {
        HttpLoggingInterceptor.Level.BODY
    } else {
        HttpLoggingInterceptor.Level.NONE
    }

    @Provides
    @Singleton
    fun providesConnectivityMonitor(context: Context): ConnectivityMonitor =
        InternetConnectivityMonitor(context)

    @Provides
    @Singleton
    fun providesNetworkConnectionInterceptor(connectivityMonitor: ConnectivityMonitor): NetworkConnectionInterceptor =
        NetworkConnectionInterceptor(connectivityMonitor)

    @Provides
    @Singleton
    fun providesOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        noNetworkInterceptor: NetworkConnectionInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(noNetworkInterceptor)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()
    }

    @Provides
    @Singleton
    fun providesRxJavaCallAdapterFactory() = RxJava2CallAdapterFactory.create()

    @Provides
    @Singleton
    fun providesRetrofit(
        gsonConverterFactory: GsonConverterFactory,
        okHttp: OkHttpClient,
        rxJavaCallAdapterFactory: RxJava2CallAdapterFactory,
        url: String
    ): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(rxJavaCallAdapterFactory)
            .baseUrl(url)
            .client(okHttp)
            .build()
    }

    @Provides
    @Singleton
    fun provideUrl(): String = BuildConfig.API_URL

    @Provides
    @Singleton
    fun providesApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}