package com.cisek.luxofttask.di.modules

import com.cisek.luxofttask.ui.REFRESH_PERIOD
import com.cisek.luxofttask.ui.REFRESH_TIME_UNIT
import dagger.Module
import dagger.Provides
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class MainModule {

    companion object {
        const val PERIOD = "period"
        const val TIME_UNIT = "time_unit"
    }

    @Provides
    @Named(PERIOD)
    @Singleton
    fun providePeriod(): Long = REFRESH_PERIOD

    @Provides
    @Named(TIME_UNIT)
    @Singleton
    fun provideTimeUnit(): TimeUnit = REFRESH_TIME_UNIT
}