package com.cisek.luxofttask.di.modules

import android.content.Context
import com.cisek.luxofttask.LuxoftTaskApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object LuxoftTaskModule {

    @Provides
    @Singleton
    @JvmStatic
    fun provideApplicationContext(application: LuxoftTaskApplication): Context =
        application.applicationContext
}