package com.cisek.luxofttask.di.modules

import com.cisek.luxofttask.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class InjectorModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity
}